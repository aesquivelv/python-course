# :snake: Curso de Python desde cero hasta ciencia de datos

En este curso, tendrás la posibilidad de aprender y/o mejorar tus habilidades para programar con Python. No es necesario tener conocimientos previos y se pretende
llegar a temas intemedio/avanzados como la ciencia de datos.

## :checkered_flag: Objetivos
Al finalizar este curso, los estudiantes podrán:
- Crear programas básicos en Python utilizando un Notebook
- Comprender cómo funciona un lenguaje de programación interpretado
- Programar estructuras de datos básicas
- Leer datos desde el teclado y/o archivos
- Resolver ejercicios utilizando Python
- Entender los principios básicos de la ciencia de datos

## :calendar: Calendario / Agenda
| # Clase | Fecha | Tema(s) | Diapositivas | Notebook (código) | Ejercicios |
| ------ | ------ | ------ | ------ | ------ | ------ |
| 00 | 06.07.2020 | Introducción, tipos de datos, leer datos desde el teclado | ----- | ----- | https://www.hackerrank.com/challenges/py-hello-world/problem|

**Nota:** este calendario se encuentra sujeto a cambios

## Material extra
A continuación se muestra material de apoyo para terminar de comprender los temas vistos en este curso

### :books: Libros
- [Python para todos: Explorando la información con Python 3](https://es.py4e.com/book)

### :pencil: Plataformas para practicar
- [HackerRank](https://www.hackerrank.com/)
- [Codeforces](http://codeforces.com/problemset)
- [OmegaUp](https://omegaup.com/)

### :notebook: Python Notebook
- [Instalar Jupyter Notebook](https://jupyter.org/install.html)
- [Google Colab](https://colab.research.google.com/)

## [![License: CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc-sa/3.0/) Licencia
En este curso se utilizará material de "Python para todos: Explorando la información con Python 3" de Charles R. Severance que se encuentra registrado bajo la licencia
Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.  Esta licencia está disponible en http://creativecommons.org/licenses/by-nc-sa/3.0/
